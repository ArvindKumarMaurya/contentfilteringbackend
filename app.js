
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./swagger.json')
var dictionary = require('./dictionary.json')
var fs = require('fs')
// var http = require('http');
var https = require('https')
var privateKey = fs.readFileSync('./keys/server.key')
var certificate = fs.readFileSync('./keys/server.crt')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0
var credentials = { key: privateKey, cert: certificate }
var app = express()
// rest API requirements
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

var getResponse = function (req, res) {
  var objectionableWords = ['fuck', 'fucking', 'fucker', 'shit', 'asshole']
  var negativeWords = ['ugly', 'bad', 'very bad', 'worst', 'not', "don't", "didn't", 'do not', 'did not', 'should not', "shouldn't", 'would not', "wouldn't"]
  var positiveWords = ['like', 'thanks', 'good', 'better', 'best', 'awesome']
  var commentString = req.query.comment // query comment
  var inputArray = commentString.toLowerCase().split('.')
  var isObjectionable = false
  var isNegative = false
  var isPositive = false
  var response = { code: 1, type: 'normal' }
  for (var singleSentence of inputArray) {
    var sentenceWords = singleSentence.split(' ')
    for (var singleWord of sentenceWords) {
      if (objectionableWords.includes(singleWord)) {
        isObjectionable = true
        response.code = 4 // returning objectionable
        response.type = 'Objectionable'
      }

      if (negativeWords.includes(singleWord)) {
        isNegative = true
        // resolve(2)
      }
    }
    if (isNegative) {
      if (singleSentence.includes('but') || singleSentence.includes('otherwise') || singleSentence.includes('apart from')) {
        // response = 2 // returning moderate
        response.code = 2 // returning moderate
        response.type = 'Moderate'
      } else {
        // response = 3 // returning negative
        response.code = 3 // returning negative
        response.type = 'Negative'
      }
    }
  }

  if ((!isNegative) && (!isObjectionable)) {
    isPositive = true
    // response = 1
    response.code = 1 // returning negative
    response.type = 'Normal'
  }

  var result = { ans: response }
  res.json(result)
}

var getCheckObjectionable = function (req, res) {
  var commentString = req.query.comment.toLowerCase() // query comment
  let negativeCount = 0
  let positiveCount = 0
  let positiveValue = 0
  let negativeValue = 0
  let isObjectionable = false

  const paraArray = commentString.split(' ')
  paraArray.forEach(element => {
    if (dictionary.objectionable[element]) {
      isObjectionable = true
    }

    if (dictionary.negative[element]) {
      negativeValue = negativeValue + dictionary.negative[element]
      negativeCount++
    }

    if (dictionary.positive[element]) {
      positiveValue = positiveValue + dictionary.positive[element]
      positiveCount++
    }
  })

  const total = positiveValue + negativeValue
  const positivePercentage = (positiveValue * 100) / (total)
  const negativePercentage = (negativeValue * 100) / (total)
  res.json({ ans: { isObjectionable: isObjectionable, Positive: positivePercentage, Negative: negativePercentage } })
  // res.json({ ans: dictionary.objectionable })
}

router.route('/contentfilter')
  // .post(createUser)
  .get(getResponse)

router.route('/checkobjectionable')
  // .post(createUser)
  .get(getCheckObjectionable)

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
app.use('/api/v1', router)
var httpServer = https.createServer(credentials, app).listen(443, () => {
  console.log('server starting on port : ' + 443)
})
module.exports = app
